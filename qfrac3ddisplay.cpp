#include "qfrac3ddisplay.h"

qFrac3Ddisplay::qFrac3Ddisplay(QWidget *parent) :
    QGLWidget(QGLFormat(QGL::DoubleBuffer  |  QGL::SampleBuffers),parent)
{
    makeCurrent();
    myShader = new QGLShaderProgram(context());
    myShader->addShaderFromSourceFile(QGLShader::Fragment,QString(":/shaders/defaults/shaders/mandelbrot.frag"));
    myShader->link();

    QPixmap colorMap(QString(":/textures/defaults/colorMaps/tex3.png"));
    colorMapTexHandle = 1;
    glBindTexture(GL_TEXTURE_1D, colorMapTexHandle);
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexImage1D(GL_TEXTURE_1D, 0, 4, 256, 0, GL_BGRA, GL_UNSIGNED_BYTE, colorMap.toImage().bits());
    scale =1.f;
    iter=300;
    center.setX(0.);
    center.setY(0.);
}



void qFrac3Ddisplay::paintGL(){
    glViewport(0,0,width(),height());
    myShader->bind();
    myShader->setUniformValue("scale",scale);
    myShader->setUniformValue("center",center);
    myShader->setUniformValue("iter",iter);
    myShader->setUniformValue("tex",0);
    myShader->setUniformValue("ratio",width()/(float) height());
    glBindTexture(GL_TEXTURE_1D,colorMapTexHandle);
    glBegin(GL_QUADS);
    glTexCoord2f(0, 0);
    glVertex2f(-1, -1);
    glTexCoord2f(1, 0);
    glVertex2f(1, -1);
    glTexCoord2f(1, 1);
    glVertex2f(1, 1);
    glTexCoord2f(0, 1);
    glVertex2f(-1, 1);
    glEnd();
}

void qFrac3Ddisplay::wheelEvent(QWheelEvent *e){
    if(e->delta()>0) scale*=1.05;
    else             scale/=1.05;
    update();
}

void qFrac3Ddisplay::keyPressEvent(QKeyEvent *e){
    QVector2D trans;
    if(e->key()==Qt::Key_Up)
        trans=QVector2D(0,0.1*scale);
    if(e->key()==Qt::Key_Down)
        trans=QVector2D(0,-0.1*scale);
    if(e->key()==Qt::Key_Left)
        trans=QVector2D(0.1*scale,0);
    if(e->key()==Qt::Key_Right)
        trans=QVector2D(-0.1*scale,0);
    center+=trans;
    update();
}
