attribute vec4 qt_Vertex;
attribute vec4 qt_MultiTexCoord0;

void main(void)
{
    gl_Position = gl_Vertex;
}
