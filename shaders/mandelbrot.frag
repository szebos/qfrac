uniform sampler1D tex;
uniform vec2 center;
uniform float scale;
uniform float ratio;
uniform int iter;

float value(int i){
    return (i == iter ? 0.0 : float(i)) / 100.0;
}

vec2 fractFunc(vec2 z, vec2 c){
    vec2 res;
    res.x = (z.x * z.x - z.y * z.y /** exp(z.y)*/) + c.x;
    res.y = (z.y * z.x + z.x * z.y /** sin(c.y)*/) + c.y;

    return res;
}

vec2 screenPos(){
    vec2 c;
    c.x = ratio * (gl_TexCoord[0].x - 0.5) * scale - center.x;
    c.y = (gl_TexCoord[0].y - 0.5) * scale - center.y;
    return c;
}

void main() {
    vec2 z, c,znext;

    c = screenPos();

    int i;
    z = c;
    for(i=0; i<iter; i++) {

        znext = fractFunc(z,c);

        if((znext.x*znext.x +znext.y*znext.y)> 4.0) break;
        z=znext;
    }

    gl_FragColor = texture1D(tex,value(i));
}
