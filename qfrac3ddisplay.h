#ifndef QFRAC3DVDISPLAY_H
#define QFRAC3DVDISPLAY_H

#include <QGLWidget>
#include <QGLShaderProgram>
#include <QVector2D>
#include <QWheelEvent>
#include <QKeyEvent>

class qFrac3Ddisplay : public QGLWidget
{
    Q_OBJECT
public:
    explicit qFrac3Ddisplay(QWidget *parent = 0);
    
signals:
    
public slots:
protected :
    virtual void paintGL();
    virtual void wheelEvent(QWheelEvent *e);
    virtual void keyPressEvent(QKeyEvent *e);
private:
    QGLShaderProgram *myShader;
    GLuint colorMapTexHandle;
    QVector2D center;
    GLfloat scale;
    int iter;
};

#endif // QFRAC3DVDISPLAY_H
