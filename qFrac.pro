#-------------------------------------------------
#
# Project created by QtCreator 2013-08-18T16:25:20
#
#-------------------------------------------------

QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = qFrac
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    qfrac3ddisplay.cpp

HEADERS  += mainwindow.h \
    qfrac3ddisplay.h

FORMS    += mainwindow.ui

OTHER_FILES += \
    default.vert \
    mandelbrot.frag \
    shaders/mandelbrot.frag \
    shaders/default.vert

RESOURCES += \
    shaders.qrc
